## ESP32 MP3 player
- can play MP3 from microSD card
- can play web radio (icecast) streams
- basic UI with touchscreen support

### Web radio stream config
Edit [stations-example.json](stations-example.json) and copy it to microSD card root<br>
**Must be named RADIO.JSO** (8.3 DOS fs limitation of embedded-sdmmc)

### Parts List
- esp32(s3 or similar) like Adafruit FeatherS3 (check partitons.csv if needed, PSRAM mandatory - check memory usage)
- Adafruit 2090 2.8 TFT LCD (ILI9341) with capacitive touch and microSD card slot in SPI mode
- PCM5102 Module Stereo DAC Digital-to-Analog converter 
- Loudspeaker or PCM to BT converter
- Button and slider switch for reset and power off

### Known issues
- limit to bitrate of max 192Kbps
- playing radio stream can lead to audio glitches due to buffer underruns (playing from microSD works pretty flawless)
- sometimes Gremlins will appear
- needs custom esp-hal https://github.com/maxwen/esp-hal/commit/b5e73680d61a22b7b40bd08fa907cada06fcdc3c
- embedded-sdmmc only supports 8.3 DOS filesystem and can be very picky with partition type<br>
Partition created as W95 FAT32 (type b in fdisk) should work

### Todo
- try to make radio stream more reliable (maybe try to use second core)
- support folders on microSD card
- more flexible web stream discovery - currently fixed list or json config on microSD

### Images
![alt text](images/PXL_20240531_112413087.NIGHT.jpg)
![alt text](images/PXL_20240531_112435038.NIGHT.jpg)